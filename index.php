<?php
    require_once 'animal.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    $sheep = new Animal("shaun");

    echo $sheep->name."  (name)<br>"; // "shaun"
    echo $sheep->legs."  (legs)<br>"; // 2
    echo $sheep->cold_blooded." (cold blooded)<br>"; // false

    echo "<br><br>";
    $sungokong = new Ape("kera sakti");
    echo $sungokong->name."  (name)<br>";
    echo $sungokong->legs."  (legs)<br>";
    echo $sungokong->yell."  (yell)<br>"; // "Auooo"
    echo $sungokong->cold_blooded."   (cold blooded)<br>";

    echo "<br><br>";
    $kodok = new Frog("buduk");
    echo $kodok->name." (name)<br>";
    echo $kodok->legs." (legs)<br>";
    echo $kodok->jump." (jump)<br>" ; // "hop hop"
    echo $kodok->cold_blooded."(cold blooded)<br>";
 ?>
